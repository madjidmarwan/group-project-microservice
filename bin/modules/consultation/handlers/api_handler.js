'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/validator');
const queryHandler = require('../repositories/queries/query_handler');
const commandHandler = require('../repositories/commands/command_handler');

const getConsultations = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParamGet(queryParam);
  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getConsultations(queryParam);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await getRequest(validateParam));
}

const getOneConsultations = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParamGet(queryParam);
  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getOneConsultations(queryParam);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await getRequest(validateParam));
}

const postOneConsultations = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParamPost(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postOneConsultations(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

const delOneConsultations = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParamGet(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.delOneConsultations(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

const putOneConsultations = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParamGet(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.putOneConsultations(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

module.exports = {
  getConsultations: getConsultations,
  getOneConsultations: getOneConsultations,
  postOneConsultations: postOneConsultations,
  delOneConsultations: delOneConsultations,
  putOneConsultations: putOneConsultations
}