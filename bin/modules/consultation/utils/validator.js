'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamPost = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.consul_id] = {length: {minimum: 4}};
    constraints[payload.fullname] = {length: {minimum: 4}};
    constraints[payload.email] = {length: {minimum: 4}};
    constraints[payload.phoneNumber] = {length: {minimum: 4}};
    constraints[payload.message] = {length: {minimum: 4}};
    values[payload.consul_id] = payload.consul_id;
    values[payload.fullname] = payload.fullname;
    values[payload.email] = payload.email;
    values[payload.phoneNumber] = payload.phoneNumber;
    values[payload.message] = payload.message;
    return await validateConstraints(values,constraints);
}

const isValidParamGet = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.consul_id] = {length: {minimum: 4}};
    constraints[payload.fullname] = {length: {minimum: 4}};
    constraints[payload.email] = {length: {minimum: 4}};
    constraints[payload.phoneNumber] = {length: {minimum: 4}};
    constraints[payload.message] = {length: {minimum: 4}};
    values[payload.consul_id] = payload.consul_id;
    values[payload.fullname] = payload.fullname;
    values[payload.email] = payload.email;
    values[payload.phoneNumber] = payload.phoneNumber;
    values[payload.message] = payload.message;
    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParamPost: isValidParamPost,
    isValidParamGet: isValidParamGet
}