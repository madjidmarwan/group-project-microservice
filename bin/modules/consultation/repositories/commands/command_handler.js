'use strict';

const Consultations = require('./domain');

const postOneConsultations = async (payload) => {
    const consultations = new Consultations();
    const postCommand = async (payload) => {
        return await consultations.addNewConsultations(payload);
    }
    return postCommand(payload);
}

const putOneConsultations = async (id, payload) => {
    const consultations = new Consultations();
    const putCommand = async (id, payload) => {
        return await consultations.updateDataConsultations(id, payload);
    }
    return putCommand(id, payload);
}

const delOneConsultations = async (id) => {
    const consultations = new Consultations();
    const putCommand = async (id) => {
        return await consultations.deleteDataConsultations(id);
    }
    return putCommand(id);
}

module.exports = {
    postOneConsultations : postOneConsultations,
    putOneConsultations : putOneConsultations,
    delOneConsultations : delOneConsultations
}