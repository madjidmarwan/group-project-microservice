'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Consultations{

    async addNewConsultations(payload){
        const data = [payload];
        let view = model.generalConsultations();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.consul_id)){accumulator.consul_id = value.consul_id;}
            if(!validate.isEmpty(value.fullname)){accumulator.fullname = value.fullname;}
            if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
            if(!validate.isEmpty(value.phoneNumber)){accumulator.phoneNumber = value.phoneNumber;}
            if(!validate.isEmpty(value.message)){accumulator.message = value.message;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneConsultations(document);
        return result;
    }

    async updateDataConsultations(payload){
        const data = [payload];
        let view = model.generalConsultations();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.consul_id)){accumulator.consul_id = value.consul_id;}
            if(!validate.isEmpty(value.fullname)){accumulator.fullname = value.fullname;}
            if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
            if(!validate.isEmpty(value.phoneNumber)){accumulator.phoneNumber = value.phoneNumber;}
            if(!validate.isEmpty(value.message)){accumulator.message = value.message;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneConsultations(document);
        return result;
    }
    
    async deleteDataConsultations(param) {
        const result = await command.deleteOneConsultations(param);
        return result;
    }
}

module.exports = Consultations;