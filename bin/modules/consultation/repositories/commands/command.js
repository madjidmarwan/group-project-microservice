'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneConsultations = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('consultations');
    const result = await db.insertOne(document);
    return result;
}

const updateOneConsultations = async (param, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('consultations');
    const result = await db.upsertOne(param ,document);
    return result;
}

const deleteOneConsultations = async (param) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('consultations');
    const result = await db.deleteOne(param);
    return result;
}

module.exports = {
    insertOneConsultations: insertOneConsultations,
    updateOneConsultations: updateOneConsultations,
    deleteOneConsultations: deleteOneConsultations
}