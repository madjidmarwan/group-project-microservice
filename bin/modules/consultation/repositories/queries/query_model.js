'use strict';

const generalConsultation = () => {
    const model = {
        consul_id:``,
        fullname:``,
        email:``,
        phoneNumber:``,
        message:``    
    }
    return model;
}

module.exports = {
    generalConsultation: generalConsultation
}
