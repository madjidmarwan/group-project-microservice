'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Consultations = require('./domain');

const getOneConsultations = async (queryParam) => {
    const validateIfExist = await validator.ifExistConsultations(queryParam);
    const getQuery = async (queryParam) => {
        const consultations = new Consultations(queryParam);
        const result = await consultations.viewOneConsultations();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}

const getConsultations = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const consultations = new Consultations(queryParam);
        const result = await consultations.viewConsultations();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

module.exports = {
    getOneConsultations : getOneConsultations,
    getConsultations : getConsultations
}