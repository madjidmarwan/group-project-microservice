'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const MySQL = require('../../../../helpers/databases/mysql/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const findConsultations = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('consultations');
    const recordset = await db.findMany();
    return recordset;
}

const findOneConsultations = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('consultations');
    const recordset = await db.findOne(parameter);
    return recordset;
}

module.exports = {
    findConsultations : findConsultations,
    findOneConsultations : findOneConsultations
}