'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParam = async (payload) => {
    let constraints = {};
    let values = {};

    constraints[payload.user_id] = {length: {minimum: 4}};
    constraints[payload.email] = {length: {minimum: 4}};
    constraints[payload.username] = {length: {minimum: 4}};
    constraints[payload.password] = {length: {minimum: 4}};
    constraints[payload.name] = {length: {minimum: 4}};
    constraints[payload.phoneNumber] = {length: {minimum: 4}};

    values[payload.user_id] = payload.user_id;
    values[payload.email] = payload.email;
    values[payload.username] = payload.username;
    values[payload.password] = payload.password;
    values[payload.name] = payload.name;
    values[payload.phoneNumber] = payload.phoneNumber;

    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParam: isValidParam
}