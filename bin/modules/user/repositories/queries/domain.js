'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Users{
    constructor(param){
        this.user_id = param.user_id;
        this.email = param.email;
        this.username = param.username;
        this.password = param.password;
        this.name = param.name;
        this.phoneNumber = param.phoneNumber;
        this.createdAt = param.createdAt;
        this.updatedAt = param.updatedAt;
    }

    async viewUsers(){
        const param = {};
        const result = await query.findUsers(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

    async viewOneUsers(){
        const param = {"user_id":this.user_id};
        const result = await query.findOneUsers(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
        // return wrapper.data(data, '', 200);
    }

    async login(){
        const param = {"username":this.username, "password": this.password};
        const result = await query.findOneUsers(param);
        
        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }
}

module.exports = Users;