'use strict';

const generalUser = () => {
    const model = {
        user_id:``,
        email:``,
        username:``,
        password:``,
        name:``,
        phoneNumber:``,     
    }
    return model;
}

module.exports = {
    generalUser: generalUser
}
