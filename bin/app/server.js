'use strict';

const restify = require('restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const corsMiddleware = require('restify-cors-middleware')
const consultationHandler = require('../modules/consultation/handlers/api_handler');
const userHandler = require('../modules/user/handlers/api_handler');

let crossOrigin = (req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    return next();
}

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['Origin, X-Requested-With, Content-Type, Accept, OPTIONS'],
    exposeHeaders: ['OPTIONS']
})

let AppServer = function(){
    this.server = restify.createServer({
        name: project.name + '-server',
        version: project.version
    });

    this.server.serverKey = '';
    this.server.pre(cors.preflight);
    this.server.use(cors.actual);
    this.server.use(restify.plugins.acceptParser(this.server.acceptable));
    this.server.use(restify.plugins.queryParser());
    this.server.use(restify.plugins.bodyParser());
    this.server.use(restify.plugins.authorizationParser());

    //required for basic auth
    this.server.use(basicAuth.init());
    this.server.use(crossOrigin);

    //anonymous can access the end point, place code bellow
    this.server.get('/', (req, res, next) => {
        wrapper.response(res,`success`,wrapper.data(`Index`),`This service is running properly`);
    });

    //authenticated client can access the end point, place code bellow
    // consultations
    this.server.post('/api/v1/get-in-touch', basicAuth.isAuthenticated, consultationHandler.postOneConsultations);
    this.server.get('/api/v1/get-in-touch/:id', basicAuth.isAuthenticated, consultationHandler.getConsultations);
    this.server.get('/api/v1/get-in-touch/:id', basicAuth.isAuthenticated, consultationHandler.getOneConsultations);
    this.server.del('/api/v1/get-in-touch/:id', basicAuth.isAuthenticated, consultationHandler.delOneConsultations);
    this.server.put('/api/v1/get-in-touch/:id', basicAuth.isAuthenticated, consultationHandler.putOneConsultations);

    // users
    this.server.post('/api/v1/user', basicAuth.isAuthenticated, userHandler.postOneUsers);
    this.server.get('/api/v1/user', basicAuth.isAuthenticated, userHandler.getUsers);
    this.server.get('/api/v1/user/:id', basicAuth.isAuthenticated, userHandler.getUsers);
    this.server.del('/api/v1/user/:id', basicAuth.isAuthenticated, userHandler.deleteOneUsers);
    this.server.put('/api/v1/user/:id', basicAuth.isAuthenticated, userHandler.putOneUsers);

    // users
    this.server.post('/api/v1/user', basicAuth.isAuthenticated);
    this.server.get('/api/v1/user', basicAuth.isAuthenticated);
    this.server.del('/api/v1/user/id', basicAuth.isAuthenticated);
    this.server.put('/api/v1/user/id', basicAuth.isAuthenticated);
}
 
module.exports = AppServer;